﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TouristWebsite.Startup))]
namespace TouristWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
